import React, { useState, useContext } from "react";
import { useNavigate, Link } from "react-router-dom";
import Login from "../pages/login/Login";
import Signup from "../pages/login/Signup";
import AuthContextProvider, { AuthContext } from "../contexts/AuthContext";
import axios from "../helpers/axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Common(props) {
  const navigate = useNavigate();
  const { activeUser, setActiveUser } = useContext(AuthContext);

  const [login, setLogin] = useState(false);
  const [signup, setSignup] = useState(false);

  const handleLogin = () => {
    setLogin(!login);
  };

  const logout = () => {
    console.log("loggin user out");

    axios
      .post("/logout")
      .then(function (response) {
        console.log(response.data);
        toast.success(response.data, {
          position: toast.POSITION.TOP_CENTER
        });

        setActiveUser({ name: "", isLoggedIn: false });
        localStorage.removeItem("userData");
      })
      .catch(function (error) {
        console.log(error);
      });

    /* setCartLength(0) */
  };

  return (
    <>
      {login ? (
        <Login
          login={login}
          setLogin={setLogin}
          signup={signup}
          setSignup={setSignup}
        />
      ) : null}
      {signup ? (
        <Signup
          login={login}
          setLogin={setLogin}
          signup={signup}
          setSignup={setSignup}
        />
      ) : null}
      <div className="site-mobile-menu site-navbar-target">
        <div className="site-mobile-menu-header">
          <div className="site-mobile-menu-close">
            <span className="icofont-close js-menu-toggle" />
          </div>
        </div>
        <div className="site-mobile-menu-body" />
      </div>
      <nav className="site-nav">
        <div className="container">
          <div className="site-navigation">
            <Link className="logo m-0" to="/">
              {" "}
              Goa Bus Tours
            </Link>
            <ul className="js-clone-nav d-none d-lg-inline-block text-left site-menu float-right">
              <li className="active">
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/feedback">Feedback</Link>
              </li>
              {activeUser.isLoggedIn ? (
                <>
                  <li className="has-children">
                    <a href="#">Hi {activeUser.name.replace(/ .*/, "")}</a>
                    <ul className="dropdown">
                      <li>
                        <a onClick={logout}>Logout</a>
                      </li>
                      <li>
                        <Link to="/bookings">Bookings</Link>
                      </li>
                      {/* {activeUser.admin ? (
                        <>
                          <li>
                            <a onClick={logout}>Admin</a>
                          </li>
                        </>
                      ) : null} */}
                    </ul>
                  </li>
                </>
              ) : (
                <>
                  <li>
                    <a onClick={handleLogin}>Login</a>
                  </li>
                </>
              )}
            </ul>
            <a
              href="#"
              className="burger ml-auto float-right site-menu-toggle js-menu-toggle d-inline-block d-lg-none light"
              data-toggle="collapse"
              data-target="#main-navbar"
            >
              <span />
            </a>
          </div>
        </div>
      </nav>

      {props.children}
    </>
  );
}

export default Common;
