import axios from "axios"

let baseURL = process.env.NODE_ENV === "development" ? "http://localhost:8080" : "https://api.website.co.in"

const axiosInstance = axios.create({
    baseURL: 'http://localhost:8080',
    withCredentials: true,
    credentials: "include",
  });



export default axiosInstance;