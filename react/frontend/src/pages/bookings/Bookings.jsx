import React, { useState, useContext, useEffect } from "react";
import axios from "../../helpers/axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AuthContextProvider, { AuthContext } from "../../contexts/AuthContext";

function Bookings() {
  const { activeUser, setActiveUser } = useContext(AuthContext);

  const [bookings, setbookings] = useState([]);

  const getData = () => {
    axios
      .get("/user-tours")
      .then(function (response) {
        // handle success
        console.log(response.data);
        let res = response.data;
        setbookings(res);
      })
      .catch(function (error) {
        // handle error
        console.log(error.response.data);
        /* alert(error.response.data) */
        toast.error(error.response.data, {
          position: toast.POSITION.TOP_CENTER
        });
      });
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <div className="hero hero-inner">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 mx-auto text-center">
              <div className="intro-wrap">
                <h1 className="mb-0">Bookings</h1>
                {/* <p className="text-white">
                  Far far away, behind the word mountains, far from the
                  countries Vokalia and Consonantia, there live the blind texts.{" "}
                </p> */}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className=" mt-5">
        <div className="d-flex justify-content-center row">
          <div className="col-md-10">
            <div className="rounded">
              <div className="table-responsive table-borderless">
                <table className="table table-striped ">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Pickup</th>
                      <th>Seats</th>
                      <th />
                    </tr>
                  </thead>
                  <tbody className="table-body">
                    {bookings.map((item, index) => {
                      return (
                        <>
                          <tr>
                            <td>{item.date}</td>
                            <td>{item.tourId[0].from}</td>
                            <td>{item.tourId[0].to}</td>
                            <td>{item.tourId[0].pickup}</td>
                            <td>{item.seats}</td>
                          </tr>
                        </>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Bookings;
