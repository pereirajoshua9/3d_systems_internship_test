import React,{useContext} from "react";
import { Helmet } from "react-helmet";
import "./login.css";
import { Formik } from "formik";
import * as Yup from "yup";
import axios from "../../helpers/axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AuthContextProvider, { AuthContext } from "../../contexts/AuthContext";



function Signup({ login, setLogin, signup, setSignup }) {
  const handleSignin = () => {
    setLogin(true);
    setSignup(false);
  };

  const { activeUser, setActiveUser } = useContext(AuthContext);

  const register = (data) => {
    console.log(data);
    axios
      .post("/register", {
        name: data.name,
        email: data.email,
        password: data.password
      })
      .then(function (response) {
        // handle success
        console.log(response.data);
        let res = response.data;

        let userData = {
          id: res._id,
          name: res.name,
          email: res.email,
          admin : res.admin,
          isLoggedIn: true
        };

        console.log("USER DATA", userData);
        localStorage.setItem("userData", JSON.stringify(userData));
        console.log("auth context", activeUser);
        setActiveUser(userData);
        setSignup(!signup);

      })
      .catch(function (error) {
        // handle error
        console.log(error.response.data);
        /* alert(error.response.data) */
        toast.error(error.response.data, {
          position: toast.POSITION.TOP_CENTER
        });
      });
  };

  return (
    <>
      {/* <div id="login-overlay" className="d-flex justify-content-center ">
        <div className="login">
          <div className="login-triangle" />
          <h2 className="login-header">Signup</h2>
          <form className="login-container">
            <p>
              <input type="text" placeholder="Name" />
            </p>
            <p>
              <input type="email" placeholder="Email" />
            </p>
            <p>
              <input type="password" placeholder="Password" />
            </p>
            <p>
              <input type="submit" defaultValue="Log in" />
            </p>
          </form>
        </div>
      </div> */}

      <Formik
        initialValues={{
          name: "",
          email: "",
          password: "",
          confirm_password: ""
        }}
        onSubmit={(values) => {
          register(values);
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string().email().required("Required"),
          name: Yup.string().required("Required"),
          password: Yup.string().required("Password is required"),
          confirm_password: Yup.string().oneOf(
            [Yup.ref("password"), null],
            "Passwords must match"
          )
        })}
      >
        {(props) => {
          const {
            values,
            touched,
            errors,
            dirty,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit,
            handleReset
          } = props;
          return (
            <div id="login-overlay" className="d-flex justify-content-center ">
              <div className="login">
                <div className="login-triangle" />
                <h2 className="login-header">SIGN UP</h2>
                <form onSubmit={handleSubmit} className="login-container">
                  <label htmlFor="name" style={{ display: "block" }}>
                    Name
                  </label>
                  <input
                    id="name"
                    placeholder="Enter your name"
                    type="text"
                    value={values.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    // className={
                    //   errors.name && touched.name
                    //     ? "text-input error "
                    //     : "text-input"
                    // }
                    className="form-control"
                  />
                  {errors.name && touched.name && (
                    <div className="input-feedback">{errors.name}</div>
                  )}

                  <label htmlFor="email" style={{ display: "block" }}>
                    Email
                  </label>

                  <input
                    id="email"
                    placeholder="Enter your email"
                    type="text"
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="form-control"
                  />
                  {errors.email && touched.email && (
                    <div className="input-feedback">{errors.email}</div>
                  )}

                  <label htmlFor="password" style={{ display: "block" }}>
                    Password
                  </label>

                  <input
                    id="password"
                    placeholder="Enter your password"
                    type="password"
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="form-control"
                  />
                  {errors.password && touched.password && (
                    <div className="input-feedback">{errors.password}</div>
                  )}

                  <label
                    htmlFor="confirm_password"
                    style={{ display: "block" }}
                  >
                    Confrim Password
                  </label>

                  <input
                    id="confirm_password"
                    placeholder="Retype password"
                    type="password"
                    value={values.confirm_password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="form-control"
                  />
                  {errors.confirm_password && touched.confirm_password && (
                    <div className="input-feedback">
                      {errors.confirm_password}
                    </div>
                  )}

                  <button
                    type="submit"
                    disabled={isSubmitting}
                    className="submit"
                  >
                    Submit
                  </button>
                  <a onClick={handleSignin} style={{ marginLeft: "195px" }}>
                    {" "}
                    Sign In{" "}
                  </a>
                </form>
              </div>
            </div>
          );
        }}
      </Formik>
    </>
  );
}

export default Signup;
