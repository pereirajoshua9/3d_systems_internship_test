import React, { useState, useContext } from "react";
import { Helmet } from "react-helmet";
import "./login.css";
import axios from "../../helpers/axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AuthContextProvider, { AuthContext } from "../../contexts/AuthContext";

function Login({ login, setLogin, signup , setSignup }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { activeUser, setActiveUser } = useContext(AuthContext);

  const handdleLogin = () => {
    axios
      .post("/login", { email, password })
      .then(function (response) {
        // handle success
        console.log(response.data[0]);
        let res = response.data[0];

        let userData = {
          id: res._id,
          name: res.name,
          email: res.email,
          admin: res.admin,
          isLoggedIn: true
        };

        console.log("USER DATA", userData);
        localStorage.setItem("userData", JSON.stringify(userData));
        console.log("auth context", activeUser);
        setActiveUser(userData);
        setLogin(!login);
      })
      .catch(function (error) {
        // handle error
        console.log(error.response.data);
        /* alert(error.response.data) */
        toast.error(error.response.data, {
          position: toast.POSITION.TOP_CENTER
        });
      });
  };

  const handleSignup = () => {
    setSignup(true)
    setLogin(false)
   
  }

  return (
    <>
      <div id="login-overlay" className="d-flex justify-content-center " >
        <div className="login">
          <div className="login-triangle" />
          <h2 className="login-header">Log in</h2>
          <div className="login-container">
            <p>
              <input
                type="email"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </p>
            <p>
              <input
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </p>
            <p>
              <button className="submit" onClick={handdleLogin}>
                Login
              </button>
            </p>
            <a onClick={handleSignup} style={{marginLeft : "200px"}}>Sign up</a>
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;
