import React, { useState, useContext } from "react";
import axios from "../../helpers/axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AuthContextProvider, { AuthContext } from "../../contexts/AuthContext";

function Feedback() {
  const { activeUser, setActiveUser } = useContext(AuthContext);

  const [feedback, setFeedback] = useState("");

  const handleSubmit = () => {
    axios
      .post("/create-feedback", { feedback })
      .then(function (response) {
        // handle success
        console.log(response.data[0]);
        let res = response.data[0];
        setFeedback("");
        toast.success("Feedback sent", {
          position: toast.POSITION.TOP_CENTER
        });
      })
      .catch(function (error) {
        // handle error
        console.log(error.response.data);
        /* alert(error.response.data) */
        toast.error(error.response.data, {
          position: toast.POSITION.TOP_CENTER
        });
      });
  };

  return (
    <>
      <div className="hero hero-inner">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 mx-auto text-center">
              <div className="intro-wrap">
                <h1 className="mb-0">Feedback</h1>
                {/* <p className="text-white">
                  Far far away, behind the word mountains, far from the
                  countries Vokalia and Consonantia, there live the blind texts.{" "}
                </p> */}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="untree_co-section">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 mb-5 mb-lg-0">
              <div className="row"></div>
              <div className="form-group">
                <label className="text-black" htmlFor="message">
                  Message
                </label>
                <textarea
                  className="form-control"
                  id="message"
                  cols={30}
                  rows={5}
                  defaultValue={""}
                  value={feedback}
                  onChange={(e) => setFeedback(e.target.value)}
                />
              </div>
              {activeUser.isLoggedIn ? (
                <>
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={handleSubmit}
                  >
                    Send Feedback
                  </button>
                </>
              ) : (
                <>
                  <button
                    type="button"
                    className="btn btn-primary"
                    disabled={true}
                  >
                    Signin to send feedback
                  </button>
                </>
              )}
            </div>
            <div className="col-lg-5 ml-auto">
              <div className="quick-contact-item d-flex align-items-center mb-4">
                <span className="flaticon-house" />
                <address className="text">
                  155 Market St #101, Paterson, NJ 07505, United States
                </address>
              </div>
              <div className="quick-contact-item d-flex align-items-center mb-4">
                <span className="flaticon-phone-call" />
                <address className="text">+1 202 2020 200</address>
              </div>
              <div className="quick-contact-item d-flex align-items-center mb-4">
                <span className="flaticon-mail" />
                <address className="text">@info@mydomain.com</address>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Feedback;
