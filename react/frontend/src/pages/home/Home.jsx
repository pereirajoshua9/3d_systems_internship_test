import React, { useState, useEffect, useContext } from "react";
import axios from "../../helpers/axios";
import Typewriter from "typewriter-effect";
import { getCurrentDate } from "../../components/getCurrentDate";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate, Link } from "react-router-dom";
import AuthContextProvider, { AuthContext } from "../../contexts/AuthContext";

function Home() {
  const [tours, setTours] = useState([]);
  const [tourId, setTourId] = useState("");
  const [date, setDate] = useState("");
  const [seats, setSeats] = useState("");
  const [avlSeats, setAvlSeats] = useState([]);
  const [available, setAvailable] = useState(false);

  const [loader, setLoader] = useState(false);

  const { activeUser, setActiveUser } = useContext(AuthContext);

  const getTours = () => {
    axios
      .get("/tours")
      .then(function (response) {
        // handle success
        console.log(response.data);
        setTours(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error.response.data);
      });
  };

  const bookTour = () => {
    setAvailable(false);
    setLoader(true);
    axios
      .post("/book-tour", { tourId: tourId, date: date, seats: seats })
      .then(function (response) {
        // handle success
        console.log(response.data);
        setAvlSeats([]);
        setAvailable(false);
        setLoader(false);
        toast.success(response.data, {
          position: toast.POSITION.TOP_CENTER
        });
      })
      .catch(function (error) {
        // handle error
        console.log(error.response.data);
      });
  };

  const checkAvailability = () => {
    setLoader(true);
    setAvailable(false);
    axios
      .post("/check-availability", { tourId: tourId, date: date, seats: seats })
      .then(function (response) {
        // handle success
        console.log(response.data);
        if (response.data.status) {
          setAvlSeats(response.data.seats);
          setAvailable(true);
          setLoader(false);
        } else {
          setLoader(false);
          toast.error("No seats available", {
            position: toast.POSITION.TOP_CENTER
          });
        }

        // toast.success(response.data, {
        //   position: toast.POSITION.TOP_CENTER
        // });
      })
      .catch(function (error) {
        // handle error
        console.log(error.response.data);
      });
  };

  useEffect(() => {
    getTours();
  }, []);

  const [mainImg, setMainImg] = useState("img1.jpg");

  useEffect(() => {
    let myArray = ["img1.jpg", "img3.jpg"];
    let count = 0;

    function cycleArray() {
      let index = count % myArray.length;
      // console.log(myArray[index]);
      setMainImg(myArray[index]);

      count++;
    }

    setInterval(cycleArray, 2000);
  }, []);

  return (
    <>
      <div className="hero">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-7">
              <div className="intro-wrap">
                <h1 className="mb-5">
                  <span className="d-block">Let's Enjoy Your</span> Ride To{" "}
                  <Typewriter
                    options={{
                      strings: [
                        "Palolem Beach",
                        "Aguada Fort",
                        "Mapusa Market",
                        "Goa State Museum"
                      ],
                      autoStart: true,
                      loop: true
                    }}
                  />
                  <span className="typed-words"></span>
                </h1>
                <div className="row">
                  <div className="col-12">
                    <div className="form">
                      <div className="row mb-2">
                        <div className="col-sm-12 col-md-6 mb-3 mb-lg-0 col-lg-4">
                          <label htmlFor="">Select Tour Route</label>
                          <select
                            name=""
                            id=""
                            className="form-control custom-select"
                            onChange={(e) => setTourId(e.target.value)}
                          >
                            <option value="">Select Tour Route</option>
                            {tours.map((item, index) => {
                              return (
                                <option key={index} value={item._id}>
                                  {item.from} TO {item.to}
                                </option>
                              );
                            })}
                          </select>
                        </div>
                        <div className="col-sm-12 col-md-6 mb-3 mb-lg-0 col-lg-3">
                          <label htmlFor="">Select Date</label>
                          <input
                            type="date"
                            min={getCurrentDate()}
                            className="form-control"
                            value={date}
                            onChange={(e) => setDate(e.target.value)}
                          />
                        </div>
                        <div className="col-sm-12 col-md-6 mb-3 mb-lg-0 col-lg-5">
                          <label htmlFor="">.</label>
                          <button
                            disabled={!tourId || !date ? true : false}
                            className="btn btn-primary btn-block"
                            onClick={checkAvailability}
                          >
                            Check Availability
                          </button>
                        </div>
                      </div>
                      {loader ? (
                        <>
                          <div className="row mb-2 d-flex justify-content-center align-items-center">
                            <div className="col-sm-12 col-md-6 mb-3 mb-lg-0 col-lg-2">
                              <img
                                src="/images/loader/loaderhome.gif"
                                alt=""
                                width="100"
                              />
                            </div>
                          </div>
                        </>
                      ) : null}

                      <br />
                      {available ? (
                        <>
                          <div className="row mb-2">
                            <div className="col-sm-12 col-md-6 mb-3 mb-lg-0 col-lg-5">
                              <label htmlFor="">
                                Number of seats available
                              </label>
                              <select
                                onChange={(e) => setSeats(e.target.value)}
                                className="form-control"
                              >
                                <option value="">No.Seats</option>
                                {avlSeats.map((item, index) => {
                                  return (
                                    <option value={item} key={index}>
                                      {item}
                                    </option>
                                  );
                                })}
                              </select>
                            </div>
                            <div className="col-sm-12 col-md-6 mb-3 mb-lg-0 col-lg-4">
                              <label htmlFor="">.</label>
                              {activeUser.isLoggedIn ? (
                                <>
                                  <button
                                    className="btn btn-primary btn-block"
                                    onClick={bookTour}
                                    disabled={!seats ? true : false}
                                  >
                                    Book Tour
                                  </button>
                                </>
                              ) : (
                                <>
                                  <button
                                    type="button"
                                    className="btn btn-primary btn-block"
                                    disabled={true}
                                  >
                                    Signin to book
                                  </button>
                                </>
                              )}
                            </div>
                          </div>
                        </>
                      ) : null}

                      {/* <div className="row align-items-center">
                        <div className="col-sm-12 col-md-6 mb-3 mb-lg-0 col-lg-5">
                          <button
                            className="btn btn-primary btn-block"
                            onClick={bookTour}
                          >
                            Book Tour
                          </button>
                        </div>
                      </div> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-5">
              <div className="slides">
                <img
                  src={"images/homepage/" + mainImg}
                  alt="Image"
                  className="img-fluid active"
                />
                <img
                  src="images/hero-slider-2.jpg"
                  alt="Image"
                  className="img-fluid "
                />
                <img
                  src="images/hero-slider-3.jpg"
                  alt="Image"
                  className="img-fluid"
                />
                <img
                  src="images/hero-slider-4.jpg"
                  alt="Image"
                  className="img-fluid"
                />
                <img
                  src="images/hero-slider-5.jpg"
                  alt="Image"
                  className="img-fluid"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="untree_co-section">
        <div className="container">
          <div className="row align-items-stretch">
            <div className="col-lg-4 order-lg-1">
              <div className="h-100">
                <div className="frame h-100">
                  <div
                    className="feature-img-bg h-100"
                    style={{
                      backgroundImage: 'url("images/hero-slider-1.jpg")'
                    }}
                  />
                </div>
              </div>
            </div>
            <div className="col-6 col-sm-6 col-lg-4 feature-1-wrap d-md-flex flex-md-column order-lg-1">
              <div className="feature-1 d-md-flex">
                <div className="align-self-center">
                  <span className="flaticon-house display-4 text-primary" />
                  <h3>Overwhelming Beaches</h3>
                </div>
              </div>
              <div className="feature-1 ">
                <div className="align-self-center">
                  <span className="flaticon-restaurant display-4 text-primary" />
                  <h3>Architectural Wonders</h3>
                </div>
              </div>
            </div>
            <div className="col-6 col-sm-6 col-lg-4 feature-1-wrap d-md-flex flex-md-column order-lg-3">
              <div className="feature-1 d-md-flex">
                <div className="align-self-center">
                  <span className="flaticon-mail display-4 text-primary" />
                  <h3>Scrumptious Seafood</h3>
                </div>
              </div>
              <div className="feature-1 d-md-flex">
                <div className="align-self-center">
                  <span className="flaticon-phone-call display-4 text-primary" />
                  <h3> Entertaining Carnival Festival</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="site-footer">
        <div className="inner first">
          <div className="container">
            <div className="row">
              <div className="col-md-6 col-lg-4">
                <div className="widget">
                  <h3 className="heading">About Bus Tour Goa </h3>
                  <p>
                    Far far away, behind the word mountains, far from the
                    countries Vokalia and Consonantia, there live the blind
                    texts.
                  </p>
                </div>
              </div>
              <div className="col-md-6 col-lg-2 pl-lg-5">
                <div className="widget">
                  <h3 className="heading">Pages</h3>
                  <ul className="links list-unstyled">
                    <li>
                      <Link to="/">Home</Link>
                    </li>
                    <li>
                      <Link to="/">Feedback</Link>
                    </li>
                    <li>
                      <Link to="/">Bookings</Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-md-6 col-lg-4">
                <div className="widget">
                  <h3 className="heading">Contact</h3>
                  <ul className="list-unstyled quick-info links">
                    <li className="email">
                      <a href="#">mail@example.com</a>
                    </li>
                    <li className="phone">
                      <a href="#">+91 9527700990</a>
                    </li>
                    <li className="address">
                      <a href="#">Office 123 abc building margao</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
