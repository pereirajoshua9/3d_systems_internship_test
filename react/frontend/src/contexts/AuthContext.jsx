import React, { createContext, useState } from 'react'

export const AuthContext = createContext();

const AuthContextProvider = (props) => {
    const [activeUser, setActiveUser] = useState({
        id : "",
        name: "",
        email: "",
        is_admin: false,
        isLoggedIn: false,
    });


    return (
        <AuthContext.Provider value={{ activeUser,setActiveUser }}>
            {props.children}
        </AuthContext.Provider>
    )
}

export default AuthContextProvider
