import { useState, useEffect,useContext } from "react";
import Common from "./layouts/Common";
import Home from "./pages/home/Home";
import Feedback from "./pages/feedback/Feedback";
import Bookings from "./pages/bookings/Bookings";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import AuthContextProvider, { AuthContext } from "./contexts/AuthContext";

function App() {
  const { activeUser, setActiveUser } = useContext(AuthContext);

  useEffect(() => {
    let userData = localStorage.getItem("userData");
    if (userData !== null) {
      setActiveUser(JSON.parse(userData));
      console.log("user is logged in ");
      /*  getCartLength() */
    }
  }, []);

  const router = createBrowserRouter([
    {
      path: "/",
      element: (
        <Common>
          <Home />
        </Common>
      )
    },
    {
      path: "/feedback",
      element: (
        <Common>
          <Feedback />
        </Common>
      )
    },
    {
      path: "/bookings",
      element: (
        <Common>
          <Bookings />
        </Common>
      )
    }
  ]);

  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default App;
