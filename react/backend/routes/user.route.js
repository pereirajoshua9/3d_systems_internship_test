const express = require("express");
const router = express.Router();
const jwtVerify = require("../helpers/jwtVerify");

const { createUser, findUser, logoutUser } = require("../Controllers/user.controller");

router.post("/login", findUser);

router.post("/register", createUser);

router.post("/logout", jwtVerify, logoutUser);

router.get("/test", jwtVerify, (req, res) => {
  res.send("auth working");
});

module.exports = router;
