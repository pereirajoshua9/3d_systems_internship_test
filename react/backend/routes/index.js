const express = require('express');

const app = express();

//routes
const userRouter = require('./user.route');
const tourRouter = require('./tour.route');
const feedbackRouter = require('./feedback.route');

app.use(userRouter);
app.use(tourRouter);
app.use(feedbackRouter);


module.exports = app;