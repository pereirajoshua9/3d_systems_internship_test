const express = require("express");
const router = express.Router();
const jwtVerify = require("../helpers/jwtVerify");

const {
  getTours,
  checkAvailability,
  bookTour,
  getUserTours
} = require("../Controllers/tour.controller");

router.get("/tours", getTours);

router.post("/check-availability", checkAvailability);

router.post("/book-tour", jwtVerify, bookTour);

router.get("/user-tours", jwtVerify, getUserTours);

module.exports = router;
