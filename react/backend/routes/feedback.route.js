const express = require("express");
const router = express.Router();
const jwtVerify = require("../helpers/jwtVerify");

const { createFeeback } = require("../Controllers/feedback.controller");


router.post("/create-feedback",jwtVerify, createFeeback);

module.exports = router;
