require("dotenv").config();
const express = require("express");
const bodyparser = require("body-parser");
const cookieParser = require("cookie-parser");
var cors = require("cors");

require("./Config/db/mongo");

const app = express();
app.use(cookieParser());
app.use(express.json());
app.use(cors({ origin: true, credentials: true }));

//api's
const api = require("./routes/index");
app.use("/", api);

app.listen(8080, () => {
  console.log("server running on port 8080!");
});

module.exports = app;
