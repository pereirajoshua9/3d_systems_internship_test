const jwt = require('jsonwebtoken');

const jwtVerify = (req, res, next) => {
  const token = req.cookies.access_token;
  // console.log(token)

  if (!token) {
    console.log("no cookie found")
    return res.sendStatus(403);
  }
  try {
    const data = jwt.verify(token, process.env.SECRETKEY);
    res.locals.userId = data.id
    /* req.userId = data.id;
    req.userRole = data.role; */
    return next();
  } catch {
    return res.sendStatus(403);
    
  }
}

module.exports = jwtVerify
