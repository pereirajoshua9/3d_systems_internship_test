const mongoose = require("mongoose");
const uri =
  "mongodb+srv://pereirajoshua9:Password%40098@cluster0.kl9ivyc.mongodb.net/3dsystem";

const userModel = require("../../Models/user.model");

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};

// Connect MongoDB Atlas using mongoose connect method
const conn = mongoose.connection;

conn.openUri(uri, options).then(
  () => {
    console.log("Database connection established!");
  },
  err => {
    {
      console.log("Error connecting Database instance due to:", err);
    }
  }
);

// run()

// async function run() {
//   let createUser = await userModel.create({
//     name: "Joshua",
//     email: "Perk@gmaio",
//     password: "asd"
//   });
// }

module.exports = conn;
