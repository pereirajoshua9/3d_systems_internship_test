require("../Config/db/mongo");
const tourModel = require("../Models/tour.model");
const userTourModel = require("../Models/usertour.model");

exports.getTours = async (req, res) => {
  try {
    let tours = await tourModel.find();

    res.status(200).json(tours);
    return;
  } catch (err) {
    console.log(err);
    res.status(500).json("try again later");
    return;
  }
};

exports.getUserTours = async (req, res) => {
  try {
    let userId = res.locals.userId;

    let tours = await userTourModel.find({userId : userId}).populate('tourId');
    console.log(tours);

    res.status(200).json(tours);
    return;
  } catch (err) {
    console.log(err);
    res.status(500).json("try again later");
    return;
  }
};

exports.checkAvailability = async (req, res) => {
  try {
    // let userId = res.locals.userId
    let { tourId, date, seats } = req.body;
    console.log(tourId);
    // console.log(date);
    // console.log(seats);

    let tour = await tourModel.findById(tourId);
    // console.log(tour)

    let usertour = await userTourModel.find({
      tourId: tourId,
      date: date
    });

    var arr = [{ x: 1 }, { x: 2 }, { x: 4 }];
    var sum = usertour.reduce(function(acc, obj) {
      return acc + obj.seats;
    }, 0);

    if (sum >= tour.seats) {
      return res.status(200).send(false);
    } else {
      let rem = [];
      for (var input = 1; input <= tour.seats - sum; input++) {
        rem.push(input);
      }

      return res.json({ status: true, seats: rem });
    }

    // console.log(usertour);
  } catch (err) {
    console.log(err);
    res.status(500).json("try again later");
    return;
  }
};

exports.bookTour = async (req, res) => {
  try {
    let userId = res.locals.userId;
    let { tourId, date, seats } = req.body;
    console.log(tourId);
    console.log(userId);
    console.log(date);
    console.log(seats);

    let usertour = await userTourModel.create({
      userId: userId,
      tourId: tourId,
      date: date,
      seats: seats
    });

    res.status(200).json("Booked Tour");
    return;
  } catch (err) {
    console.log(err);
    res.status(500).json("try again later");
    return;
  }
};
