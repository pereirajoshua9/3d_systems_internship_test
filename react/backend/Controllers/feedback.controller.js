require("../Config/db/mongo");
const feedbackModel = require("../Models/feedback.model ");

exports.createFeeback = async (req, res) => {
  console.log("create feeback api hit")
  try {
    let { feedback } = req.body;
    let userId = res.locals.userId

    console.log(feedback);
    console.log(userId);

    if (!feedback) {
      return res.status(422).send("Enter feedback");
    }

    let createFeeback = await feedbackModel.create({
      userId: userId,
      feedback: feedback
    });

    console.log(createFeeback);

    if (!createFeeback) {
      res.status(400).json("Please try again later");
      return;
    }

    res.status(200).json(createFeeback);
    return;
  } catch (err) {
    console.log(err);
    res.status(500).json("try again later");
    return;
  }
};