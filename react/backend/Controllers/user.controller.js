require("../Config/db/mongo");
const userModel = require("../Models/user.model");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

exports.createUser = async (req, res) => {
  try {
    let { name, email } = req.body;
    let password = bcrypt.hashSync(req.body.password, 10);

    console.log(name, email, password);

    if (!email) {
      return res.status(422).send("Enter email...");
    }

    if (!password) {
      console.log("enter password");
      return res.status(422).send("Enter password...");
    }

    let findUser = await userModel.find({
      email: email
    });


    if(findUser.length > 0){
      return res.status(422).send("User with entered email exists");
    }

    let createUser = await userModel.create({
      name: name,
      email: email,
      password: password,
      admin: false
    });

    if (!createUser) {
      res.status(400).json("Please try again later");
      return;
    }
    console.log(createUser);

    const token = jwt.sign({ id: createUser._id }, process.env.SECRETKEY);

    res
      .cookie("access_token", token, {
        sameSite: "strict",
        maxAge: 60 * 60 * 8760000, // 1 year
        /* path: '/', */
        /* expires: new Date(new Date().getTime() + 100 * 1000), */
        httpOnly: true
      })
      .status(200)
      .json(createUser);
    return;
  } catch (err) {
    console.log(err);
    res.status(500).json("try again later");
    return;
  }
};

exports.findUser = async (req, res) => {
  console.log("login route hit");

  try {
    let { email, password } = req.body;

    console.log(email, password);

    if (!email) {
      return res.status(422).send("Enter email...");
    }

    if (!password) {
      console.log("enter password");
      return res.status(422).send("Enter password...");
    }

    let createUser = await userModel.find({
      email: email
    });

    if (!bcrypt.compareSync(password, createUser[0].password)) {
      return res.status(422).send("Incoorect password...");
    }

    if (!createUser) {
      res.status(400).json("Try Again");
      return;
    }

    if (createUser.length == 0) {
      res.status(400).json("User not found");
      return;
    }

    const token = jwt.sign({ id: createUser[0]._id }, process.env.SECRETKEY);

    res
      .cookie("access_token", token, {
        sameSite: "strict",
        maxAge: 60 * 60 * 8760000, // 1 year
        /* path: '/', */
        /* expires: new Date(new Date().getTime() + 100 * 1000), */
        httpOnly: true
      })
      .status(200)
      .json(createUser);
    return;
  } catch (err) {
    console.log(err);
    res.status(500).json("try again later");
    return;
  }
};

exports.logoutUser = async (req, res) => {
  console.log("logout route hit");

  try {
    return res
      .clearCookie("access_token")
      .status(200)
      .json("Successfully logged out");
  } catch (err) {
    console.log(err);
    res.status(500).json("try again later");
    return;
  }
};
