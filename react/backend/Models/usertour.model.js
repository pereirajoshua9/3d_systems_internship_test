const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserTourSchema = new Schema({
  userId: {
    type: [{ type: Schema.Types.ObjectId, ref: "User" }],
    required: true
  },
  tourId: {
    type: [{ type: Schema.Types.ObjectId, ref: "Tour" }],
    required: true
  },
  date: {
    type: String,
    required: true
  },
  seats: {
    type: Number,
    required: true
  }
});

module.exports = mongoose.model("UserTour", UserTourSchema);
