const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TourSchema = new Schema({
  from: {
    type: String,
    required: true
  },
  to: {
    type: String,
    required: true
  },
  seats: {
    type: Number,
    required: true
  },
  pickup: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model("Tour", TourSchema);
