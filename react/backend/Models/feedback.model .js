const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FeedbackSchema = new Schema({
  userId: {
    type: [{ type: Schema.Types.ObjectId, ref: "User" }],
    required: true
  },
  feedback: {
    type: String,
    required : true,
  }
});

module.exports = mongoose.model("Feedback", FeedbackSchema);
