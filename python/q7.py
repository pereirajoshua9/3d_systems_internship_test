from bs4 import BeautifulSoup
import requests


def get_links(url):
    try:
        res = requests.get(url)

        soup = BeautifulSoup(res.content, 'html.parser')
        links = []

        for link in soup.findAll('a'):
            # print(link.get('href'))
            links.append(link.get('href'))

        return links
    except requests.exceptions.RequestException as e:
        print("Error fetching the webpage:", e)
        return []



url = "https://www.3dsystems.com/"

for i in get_links(url):
    print(i)

    