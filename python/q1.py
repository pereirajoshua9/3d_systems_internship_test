def is_palindrome(a):
    reverse = a[::-1]
    palindrome = True

    i = 0

    while(i < len(a)):
        if reverse[i] != a[i]:
            palindrome = False
            break
        i = i +1

    return palindrome


print(is_palindrome("joshua"))

