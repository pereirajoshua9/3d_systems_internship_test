def fibonacci(n :int):
    num1 = 0
    num2 = 1
    next = num2
    count = 1
    list = []
    while count <= n:
        list.append(num1)
        # print(num1)
        count = count + 1
        num1, num2 = num2, next
        next = num1 + num2
    return list


print(fibonacci(10))
