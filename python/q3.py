def parse_csv(csv_content):
    dictionaries = []
    lines = csv_content.strip().split('\n')

    if not lines:
        return dictionaries
    
    head = lines[0].split(',')
    
    for i in range(1,len(lines)):
        line = lines[i].split(',')
        temp_dict = {}
        for key in range(0,len(line)):
            temp_dict[head[key]] = line[key]
        dictionaries.append(temp_dict)
    
    return dictionaries





csv_data = """
name,age,roll_no
John,18,1
tom,20,2
norman,22,3
joshua,22,4
jayson,25,5
castor,28,6
qaiser,25,7
"""

print(parse_csv(csv_data))
