import requests

def get_city(city):
    api = "f839e5eafca45a1f5702d72689f7bb33"

    url = "http://api.openweathermap.org/geo/1.0/direct?q="+city+"&limit=1&appid="+api
    res = requests.get(url)

    res_object = res.json()
    if res_object == []:
        return "No city found"
    else:
        return res_object[0]

def get_weather(lat,lon):
    lat = 20.4989946
    lon = 73.8282141

    api = "f839e5eafca45a1f5702d72689f7bb33"
    url = "https://api.openweathermap.org/data/2.5/weather?lat="+str(lat)+"&lon="+str(lon)+"&appid="+api
    res = requests.get(url)
    res_object = res.json()

    return res_object["weather"][0]
    

city = input("enter city: ")

city_obj = get_city(city)

lat = city_obj["lat"]
lon = city_obj["lon"]

wether_obj = get_weather(lat,lon)

print("City:",city_obj["name"])
print("weather: ", wether_obj["main"])
print("weather description: ", wether_obj["description"])