class Rectangle:
    def __init__(self, width, height):
        self.width = width
        self.height = height
    
    def rectangle_area(self):
        return self.width * self.height
    
    def rectangle_perimeter(self):
        return 2 * (self.width + self.height)
    


rectangle = Rectangle(8, 10)

area = rectangle.calculate_area()
perimeter = rectangle.calculate_perimeter()

print("Area:", area)
print("Perimeter:", perimeter)
