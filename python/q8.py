from typing import Union

def safe_divide(a: float, b: float):
    try:
        result = a / b
        return result
    except ZeroDivisionError:
        return "You cannot devide a number by zero"

a = 10.0
b = 2.0

result = safe_divide(a, b)
print(result)

result = safe_divide(a, 0)
print(result)
