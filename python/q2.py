def copy_file(s,d):
    import os

    try:
        a = open(s, "r")
        if os.path.exists(d):
            print("destination file exists")
        else:
            open(d, "w").write(a.read())
    except:
        print("source not found")

copy_file("file1.txt","/home/josh/file2.txt")